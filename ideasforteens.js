var express = require('express');
var app = express();
app.get('/', function (req, res) {
    res.send('Hello World!');
});
app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});

var corpora = require('corpora-project');
var Twit = require('twit');
var T = new Twit(require('./config.js'));

Array.prototype.pick = function() {
    return this[Math.floor(Math.random() * this.length)];
};

Array.prototype.pickRemove = function() {
    var index = Math.floor(Math.random() * this.length);
    return this.splice(index, 1)[0];
};

function tweet() {
    var types = [
        { type: 'music',        file: 'genres',         val: 'genres' },
        { type: 'film-tv',      file: 'tv_shows',       val: 'tv_shows' },
        { type: 'art',          file: 'isms',           val: 'isms' },
        { type: 'technology',   file: 'appliances',     val: 'appliances' },
        { type: 'corporations', file: 'fortune500',     val: 'companies' },
        { type: 'foods',        file: 'condiments',     val: 'condiments' },
        { type: 'foods',        file: 'herbs_n_spices', val: 'herbs' },
        { type: 'foods',        file: 'herbs_n_spices', val: 'spices' },
        { type: 'foods',        file: 'herbs_n_spices', val: 'mixtures' },
        { type: 'words',        file: 'nouns',          val: 'nouns' }
    ];

    var rand = Math.floor(Math.random() * types.length);

    var names = corpora.getFile(types[rand].type, types[rand].file)[types[rand].val];

    var connector = 'for'
    switch(rand) {
        case 0:
            connector = 'with'
            break;
    }
    
    var newTweet = names.pick() + ', but ' + connector + ' Teens.'
    console.log(newTweet);

    T.post('statuses/update', { status: newTweet }, function(err, reply) {
        if (err) {
            console.log('error:', err);
        }
        else {
            console.log('reply:', reply);
        }
    });
}

tweet();

setInterval(function () {
    try {
        tweet();
    }
    catch (e) {
        console.log(e);
    }
}, 1000 * 60 * 5);